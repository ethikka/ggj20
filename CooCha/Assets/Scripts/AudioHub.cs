﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHub : MonoBehaviour
{
    public AudioSource AudioSourceTemplate;

    public AudioSource GenerateAudioSource()
    {
        return Instantiate<AudioSource>(AudioSourceTemplate, gameObject.transform);
    }

    Dictionary<string, AudioSource> _audioSources = new Dictionary<string, AudioSource>();

    public void PlayOnce(AudioClip clip, string uniqueKey, float volume)
    {
        if (clip == null) return;
        if (!_audioSources.ContainsKey(uniqueKey))
            _audioSources[uniqueKey] = Instantiate<AudioSource>(AudioSourceTemplate, gameObject.transform);
        _audioSources[uniqueKey].clip = clip;
        _audioSources[uniqueKey].loop = false;
        _audioSources[uniqueKey].volume = volume;
        _audioSources[uniqueKey].Play();
    }

    public void PlayLooped(AudioClip clip, string uniqueKey, float volume)
    {
        if (clip == null) return;
        if (!_audioSources.ContainsKey(uniqueKey))
            _audioSources[uniqueKey] = Instantiate<AudioSource>(AudioSourceTemplate, gameObject.transform);
        _audioSources[uniqueKey].clip = clip;
        _audioSources[uniqueKey].loop = true;
        _audioSources[uniqueKey].volume = volume;
        _audioSources[uniqueKey].Play();
    }

    public bool IsPlaying(string uniqueKey)
    {
        if (!_audioSources.ContainsKey(uniqueKey)) return false;
        return _audioSources[uniqueKey].isPlaying;
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}