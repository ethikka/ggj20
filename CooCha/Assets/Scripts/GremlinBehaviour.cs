﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GremlinBehaviour : MonoBehaviour
{
    AudioHub _audioHub;
    public List<AudioClip> attackSounds;
    public List<AudioClip> snickers;

    public Generator generator;
    public GameObject target;
    private float movementSpeed;
    private float breakTime;
    private float force;
    // Start is called before the first frame update
    void Start()
    {
        _audioHub = GameObject.FindObjectOfType<AudioHub>();
        var s = GameObject.Find("Gameplay").GetComponent<GameplaySettings>();
        movementSpeed = s.GremlinSpeed;
        breakTime = s.BreakTime;
        force = s.WhackForce;

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "WingTag")
        {
           Physics.IgnoreCollision(collision.collider, this.gameObject.GetComponent<CapsuleCollider>());

        }
    }

    private bool breaking;
    private System.DateTime breakMoment;

    public bool IsBreaking()
    {
        return breaking;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == target)
        {
            target.GetComponent<TargetBehaviour>().Break();
            breakMoment = System.DateTime.Now;
            breaking = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        breaking = false;
        target.GetComponent<TargetBehaviour>().Unbreak();
    }

    private void OnBecameInvisible()
    {
        Destroy(target);
        Destroy(this.gameObject);
    }

    private System.DateTime nextSnickerTime = System.DateTime.Now.AddMilliseconds(400);

    // Update is called once per frame
    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {


            if (System.DateTime.Now > nextSnickerTime)
            {
                _audioHub.PlayOnce(snickers[Random.Range(0, snickers.Count)], GetInstanceID().ToString() + "snicker", 0.7f);
                nextSnickerTime = System.DateTime.Now.AddMilliseconds(Random.Range(400, 10000));
            }

            if (breaking)
            {
                if ((System.DateTime.Now - breakMoment).TotalMilliseconds > (breakTime * 1000))
                {
                    breaking = false;
                    target.GetComponent<TargetBehaviour>().Broken();
                    target = null;
                }
                else
                {
                    if (!_audioHub.IsPlaying(GetInstanceID().ToString() + "attack"))
                        _audioHub.PlayOnce(attackSounds[Random.Range(0, attackSounds.Count)], GetInstanceID().ToString() + "attack", 0.5f);
                }
            }
            if (target == null)
            {
                breaking = false;
                target = generator.SpawnTarget(this.gameObject);
            }
            else
            {
                Vector3 t = target.transform.position;
                t.y = transform.position.y;
                transform.position = Vector3.MoveTowards(transform.position, t, movementSpeed * Time.deltaTime);
            }
        }
    }

    public void onHit(GameObject player)
    {
        Vector3 direction = this.gameObject.transform.position - player.transform.position;
        this.gameObject.GetComponent<Rigidbody>().AddForce(direction * force, ForceMode.Impulse);
    }
}
