﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackdropScroller : MonoBehaviour
{
    private Renderer _sh;
    float scrollSpeed = -0.5f;

    // Start is called before the first frame update
    void Start()
    {
      _sh = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {
            Vector2 textureOffset = new Vector2(Time.time * scrollSpeed, 0);
            _sh.material.mainTextureOffset = textureOffset;
        }
    }
}
