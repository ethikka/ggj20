﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour
{
    public float shake_intensity;

    private Quaternion original_rotation;
    private Vector3 origin_position;
    bool getpos = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {
            if (!getpos)
            {
                origin_position = this.gameObject.transform.position;
                shake_intensity = .03f;
                original_rotation = this.gameObject.transform.rotation;
                getpos = true;
            }

            shake_intensity = 0;
            foreach (var o in GameObject.FindObjectsOfType<GremlinBehaviour>())
                if (o.IsBreaking()) shake_intensity += 0.01f;

            foreach (var o in GameObject.FindObjectsOfType<TargetBehaviour>())
                if (o.IsBroken()) shake_intensity += 0.005f;

            shake_intensity = Mathf.Clamp(shake_intensity, 0, 0.03f);
            transform.position = origin_position + Random.insideUnitSphere * shake_intensity;
            transform.rotation = new Quaternion(
            original_rotation.x + Random.Range(-shake_intensity, shake_intensity) * .2f,
            original_rotation.y + Random.Range(-shake_intensity, shake_intensity) * .2f,
            original_rotation.z + Random.Range(-shake_intensity, shake_intensity) * .2f,
            original_rotation.w + Random.Range(-shake_intensity, shake_intensity) * .2f);
        }
    }
}
