﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : MonoBehaviour
{
    AudioHub _audioHub;

    public AudioClip EngineChoke;
    public AudioClip EngineStart;
    public AudioClip EngineRun;
    public AudioClip EngineStall;

    // Start is called before the first frame update
    void Start()
    {
        _audioHub = GameObject.FindObjectOfType<AudioHub>();
        AdvanceState();
    }

    enum EngineState
    {
        esRunning,
        esStall,
        esChoke,
        esRun
    }

    private EngineState _state = EngineState.esRun;
    private void AdvanceState()
    {
        _state = (EngineState)(((int)_state + 1) % 4);
        switch (_state)
        {
            case EngineState.esRunning: _audioHub.PlayLooped(EngineRun, GetInstanceID().ToString(), 0.2f); break;
            case EngineState.esStall: _audioHub.PlayOnce(EngineStall, GetInstanceID().ToString(), 0.2f); break;
            case EngineState.esChoke: _audioHub.PlayOnce(EngineChoke, GetInstanceID().ToString(), 0.2f); break;
            case EngineState.esRun: _audioHub.PlayOnce(EngineStart, GetInstanceID().ToString(), 0.2f); break;
        }
    }

    int chokeCounter = 0;
    // Update is called once per frame
    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {
            int angle = 15;
            switch (_state)
            {
                case EngineState.esRunning: if (Random.Range(0, 1000) == 634) AdvanceState(); break;
                case EngineState.esStall: angle = 0; if (!_audioHub.IsPlaying(GetInstanceID().ToString())) AdvanceState(); break;
                case EngineState.esChoke:
                    angle = 3;
                    if (!_audioHub.IsPlaying(GetInstanceID().ToString()))
                    {
                        if (chokeCounter > Random.Range(0, 2))
                        {
                            chokeCounter = 0;
                            AdvanceState();
                        }
                        else
                        {
                            chokeCounter++;
                            _audioHub.PlayOnce(EngineChoke, GetInstanceID().ToString(), 0.2f);
                        }
                    }
                    break;
                case EngineState.esRun: if (!_audioHub.IsPlaying(GetInstanceID().ToString())) AdvanceState(); break;
            }
            this.gameObject.transform.Rotate(0, 0, angle);
        }
    }
}
