﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneState
{
    ssPreAmble,
    ssPlay,
    ssPauze,
    ssGameOver,
    ssWinner
}

public static class Level
{
    public static SceneState State;
}

public class GameplaySettings : MonoBehaviour
{

    public float GremlinCooldown = 1.0f;
    public float GremlinSpeed = 1.0f;
    public int GremlinCount = 2;
    public float BreakTime = 3.0f;

    public float RepairTime = 5.0f;
    public float PlayerSpeed = 1.0f;
    public float WhackForce = 80.0f;

    public GameObject endpos_cam;
    public GameObject main_cam;

    public AudioClip music;

    // Start is called before the first frame update
    void Start()
    {
        Level.State = SceneState.ssPreAmble;
        GameObject.FindObjectOfType<AudioHub>().PlayLooped(music, "music", 0.9f);
    }

    // Update is called once per frame
    bool lerped = false;

    void Update()
    {
        if (!lerped)
        {
            main_cam.transform.position = Vector3.Lerp(main_cam.transform.position, endpos_cam.transform.position, 0.3f * Time.deltaTime);
            float dist = Vector3.Distance(main_cam.transform.position, endpos_cam.transform.position);
            if (dist < 0.1f)
            {
                lerped = true;
                Level.State = SceneState.ssPlay;
                main_cam.transform.position = endpos_cam.transform.position;
            }
        }

        if (Input.GetKeyDown(KeyCode.F1)) SceneManager.LoadScene("StartMenu", LoadSceneMode.Single);
        if (Input.GetKeyDown(KeyCode.F5)) Level.State = SceneState.ssPlay;
        if (Input.GetKeyDown(KeyCode.F8)) Level.State = SceneState.ssPauze;
    }
}
