﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    class gremlinInfo
    {
        public System.DateTime DiedAt;
        public GameObject obj;
    }

    public GameObject possibleTargetRange;
    public GameObject TargetPrefab;
    public GameObject GremlinPrefab;

    public List<GameObject> SpawnPoints = new List<GameObject>();

    void Start()
    {
        settings = gameObject.GetComponent<GameplaySettings>();
        while (_gremlins.Count < settings.GremlinCount)
            _gremlins.Add(new gremlinInfo());
    }

    private GameplaySettings settings;
    private List<gremlinInfo> _gremlins = new List<gremlinInfo>();
    private System.DateTime lastSpawn;

    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {
            foreach (var g in _gremlins)
            {
                var d = System.DateTime.Now;
                if (g.obj == null)
                {
                    if ((d - lastSpawn).TotalMilliseconds > ((settings.GremlinCooldown / 2) * 1000))
                        if ((d - g.DiedAt).TotalMilliseconds > (settings.GremlinCooldown * 1000))
                        {
                            g.obj = SpawnGremlin();
                            lastSpawn = d;
                        }

                }
                else
                    g.DiedAt = d;
            }
        }
    }

    public GameObject SpawnTarget(GameObject gremlin)
    {
        var o = Instantiate(TargetPrefab);

        float minx = 9999999f, maxx = -9999999f, minz = 9999999f, maxz = -9999999f;
        foreach (var p in SpawnPoints)
        {
            minx = Mathf.Min(minx, p.transform.position.x);
            maxx = Mathf.Max(maxx, p.transform.position.x);
            minz = Mathf.Min(minz, p.transform.position.z);
            maxz = Mathf.Max(maxz, p.transform.position.z);
        }
        var xx = new Vector3(Random.Range(minx, maxx), 1.10f, Random.Range(minz, maxz));
        while (Vector3.Distance(xx, gameObject.transform.position) < 0.1f)
            xx = new Vector3(Random.Range(minx, maxx), 1.10f, Random.Range(minz, maxz));

        o.transform.position = xx;
        return o;
    }

    private GameObject SpawnGremlin()
    {
        var g = Instantiate(GremlinPrefab);
        var p = Random.Range(0, SpawnPoints.Count);
        g.transform.position = Vector3.MoveTowards(SpawnPoints[p].transform.position, SpawnPoints[(p + 1) % 4].transform.position, Random.Range(0, Vector3.Distance(SpawnPoints[p].transform.position, SpawnPoints[(p + 1) % 4].transform.position)));
        g.GetComponent<GremlinBehaviour>().target = SpawnTarget(g);
        g.GetComponent<GremlinBehaviour>().generator = this;
        return g;
    }
}
