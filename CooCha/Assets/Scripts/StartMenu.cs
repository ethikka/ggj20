﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    System.DateTime canSkip;

    // Start is called before the first frame update
    void Start()
    {
        canSkip = System.DateTime.Now.AddSeconds(6);
    }

    // Update is called once per frame
    void Update()
    {
        if (System.DateTime.Now > canSkip)
            if (Input.anyKey)
                SceneManager.LoadScene("Timo", LoadSceneMode.Single);
    }
}
