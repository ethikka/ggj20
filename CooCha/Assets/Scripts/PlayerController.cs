﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    private float speed;
    public float gravity = 20.0F;

    private Vector3 moveDirection = Vector3.zero;
    public CharacterController controller;


    private GremlinBehaviour _gremlin;
    private GameObject _target;
    private void OnCollisionEnter(Collision collision)
    {
        var t = collision.gameObject.GetComponent<GremlinBehaviour>();
        if (t != null) _gremlin = t;
    }

    private void OnCollisionExit(Collision collision)
    {
        _gremlin = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TargetBehaviour>() != null)
            _target = other.gameObject;
    }

    void Start()
    {
        this.speed = GameObject.Find("Gameplay").GetComponent<GameplaySettings>().PlayerSpeed;

        controller = GetComponent<CharacterController>();
    }

    private bool _repairing;
    private System.DateTime _repairingSince;
    void Update()
    {
        if (Level.State == SceneState.ssPlay)
        {


            // Character is on ground (built-in functionality of Character Controller)
            if (controller.isGrounded)
            {
                // Use input up and down for direction, multiplied by speed
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;
            }
            // Apply gravity manually.
            moveDirection.y -= gravity * Time.deltaTime;
            // Move Character Controller
            controller.Move(moveDirection * Time.deltaTime);

            if (Input.GetButtonDown("Fire1"))
            {
                if (_gremlin != null)
                    _gremlin.onHit(this.gameObject);
            }

            _repairing = Input.GetButton("Fire2");
            if (!_repairing) _repairingSince = System.DateTime.Now;
            else
            {
                if ((System.DateTime.Now - _repairingSince).TotalMilliseconds > (3000))
                {
                    if (_target != null)
                    {
                        Destroy(_target);
                        _repairingSince = System.DateTime.Now;
                    }
                }
            }
        }
    }
}