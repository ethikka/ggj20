﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBehaviour : MonoBehaviour
{
    public ParticleSystem Smoke;
    public ParticleSystem Sparks;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Repair()
    {
        Sparks.Stop();
        Smoke.Stop();
        _broken = false;
    }

    public void Break()
    {
        if (Sparks != null)
          Sparks.Play();
    }

    public void Unbreak()
    {
        Sparks.Stop();
    }

    private bool _broken;
    public bool IsBroken()
    {
        return _broken;
    }

    public void Broken()
    {
        _broken = true;
//        Sparks.Stop();
        Smoke.Play();
    }
}
