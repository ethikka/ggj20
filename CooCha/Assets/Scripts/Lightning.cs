﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public float StormFactor = 1.0f;

    private AudioHub _audioHub;

    Light light2;
    public List<AudioClip> thunderSounds;
    // Start is called before the first frame update
    void Start()
    {
        _audioHub = GameObject.FindObjectOfType<AudioHub>();
        light2 = gameObject.GetComponent<Light>();
        StartCoroutine(DoOnOff());
    }

    // Update is called once per frame
    void Update()
    {

        
    }

    IEnumerator DoOnOff()
    {
        while (true)
        {
            light2.intensity = 0;
            yield return new WaitForSeconds(Random.Range(1, 10));
            var numFlashes = Random.Range(2, 6);
            if (thunderSounds.Count > 0)
              _audioHub.PlayOnce(thunderSounds[Random.Range(0, thunderSounds.Count)], "thunder", 1.0f);
            for (int i = 0; i < numFlashes; i++)
            {
                light2.intensity = StormFactor;
                yield return new WaitForSeconds(0.05f);
                light2.intensity = 0;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
